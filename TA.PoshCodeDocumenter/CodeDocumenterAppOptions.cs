// This file is part of the TA.PoshCodeDocumenter project
// 
// Copyright � 2015-2016 Tigra Networks., all rights reserved.
// 
// File: CodeDocumenterAppOptions.cs  Last modified: 2016-09-08@01:13 by Tim Long

using System.Collections.Generic;
using CommandLine;

namespace TA.PoshCodeDocumenter
    {
    internal class CodeDocumenterAppOptions
        {
        [Option('d', "directory", DefaultValue = ".", HelpText = "The directory to be scanned")]
        public string RootDirectory { get; set; }

        [Option('r', "recurse-subdirectories", DefaultValue = false, HelpText = "Scan subdirectories recursively")]
        public bool RecurseSubdirectories { get; set; }

        [Option('i', "include", DefaultValue = new[] {"cs", "cshtml"},
            HelpText = "List of file extensions to include")]
        public IList<string> IncludeExtensions { get; set; }

        [Option('t', "template", Required = false, HelpText = "Specify a Word Template (.dotx) file containing styles")]
        public string Template { get; set; }
        }
    }