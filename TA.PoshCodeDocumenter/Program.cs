﻿// This file is part of the TA.PoshCodeDocumenter project
// 
// Copyright © 2015-2016 Tigra Networks., all rights reserved.
// 
// File: Program.cs  Last modified: 2016-08-29@21:39 by Tim Long

using System;

namespace TA.PoshCodeDocumenter
    {
    internal class Program
        {
        private static void Main(string[] args)
            {
            try
                {
                var app = new CodeDocumenterApp(args);
                app.Run();
                Console.WriteLine("Run completed.");
                }
            catch (Exception ex) // Global exception handler
                {
                Console.WriteLine("Application error: {0}", ex.Message);
                if (Environment.ExitCode >= 0)
                    Environment.ExitCode = ex.HResult < 0 ? ex.HResult : -1; // Unspecified error
                }
#if DEBUG
            Console.ReadLine();
#endif
            }
        }
    }