﻿// This file is part of the TA.PoshCodeDocumenter project
// 
// Copyright © 2015-2016 Tigra Networks., all rights reserved.
// 
// File: WordDocumentExtensions.cs  Last modified: 2016-09-08@01:42 by Tim Long

using System.IO;
using Novacode;

namespace TA.PoshCodeDocumenter
    {
    internal static class WordDocumentExtensions
        {
        public static void AddFileHeading(this DocX document, string heading)
            {
            var paragraph = document.InsertParagraph();
            paragraph.StyleName = "CodeHeading";
            paragraph.Append(heading);
            //paragraph.Font(FontFamily.GenericSansSerif).FontSize(12).Bold();
            //paragraph.InsertPageBreakBeforeSelf();
            }

        public static void AppendCodeFileWithLineNumbers(this DocX document, string filename)
            {
            var fileLines = File.ReadAllLines(filename);
            var lineNumber = 0;
            var para = document.InsertParagraph();
            para.StyleName = "Code";

            foreach (var fileLine in fileLines)
                {
                ++lineNumber;
                para.AppendLineOfCode(lineNumber, fileLine);
                }
            }

        public static void AppendLineOfCode(this Paragraph para, int lineNumber, string text)
            {
            para.AppendLine($"{lineNumber:D4}\t{text}");
            //.Font(FontFamily.GenericMonospace)
            //.FontSize(7);
            }
        }
    }