// This file is part of the TA.PoshCodeDocumenter project
// 
// Copyright � 2015-2016 Tigra Networks., all rights reserved.
// 
// File: CodeDocumenterApp.cs  Last modified: 2016-09-08@01:14 by Tim Long

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;
using Novacode;

namespace TA.PoshCodeDocumenter
    {
    internal class CodeDocumenterApp
        {
        private readonly string[] commandLineArguments;
        private ParserResult<CodeDocumenterAppOptions> options;

        public CodeDocumenterApp(string[] commandLineArguments)
            {
            this.commandLineArguments = commandLineArguments;
            }

        public void Run()
            {
            /*
             * Process the command line arguments
             */
            var caseInsensitiveParser = new Parser(with =>
                {
                with.CaseSensitive = false;
                with.IgnoreUnknownArguments = false;
                with.HelpWriter = Console.Error;
                });
            var errorMessages = new List<string>();
            try
                {
                options = caseInsensitiveParser.ParseArguments<CodeDocumenterAppOptions>(commandLineArguments);
                if (options.Errors.Any())
                    {
                    errorMessages.Add("Unable to process command line arguments.");
                    }
                }
            catch (Exception ex)
                {
                errorMessages.Add(ex.Message);
                }
            if (errorMessages.Any())
                {
                foreach (var errorMessage in errorMessages)
                    {
                    Console.WriteLine(errorMessage);
                    }
                Environment.Exit(-1);
                }
            DocumentCode(options.Value);
            }

        /*
         * Strategy:
         * 1. Enumerate all the files and store paths relative to the root.
         * 2. Create Word document for results
         * 3. Iterate through files:
         *      - Read the entire file contents
         *      - Split into lines
         *      - output page break
         *      - output page header consisting of the file name relative to root
         *      - output each line prefixed by a line number.
         * 4. Save the document
         */

        private void DocumentCode(CodeDocumenterAppOptions options)
            {
            var filenames = EnumerateFiles(options.RootDirectory, options.IncludeExtensions,
                options.RecurseSubdirectories);
            var document = CreateWordDocument(@".\Code.docx", options.Template);
            foreach (var filename in filenames)
                {
                Console.WriteLine($"File: {filename}");
                DocumentOneFile(document, options.RootDirectory, filename);
                }
            document.Save();
            }

        private void DocumentOneFile(DocX document, string rootDirectory, string relativeFilename)
            {
            var fullyQualifiedFileName = Path.Combine(rootDirectory, relativeFilename.TrimStart('\\'));
            document.AddFileHeading(relativeFilename);
            document.AppendCodeFileWithLineNumbers(fullyQualifiedFileName);
            }

        private DocX CreateWordDocument(string outputFileName, string template = null)
            {
            var document = DocX.Create(outputFileName);
            if (!string.IsNullOrWhiteSpace(template) && File.Exists(template))
                document.ApplyTemplate(template, false);
            document.InsertParagraph(); // Ensures the document always contains at least one para
            return document;
            }

        private IEnumerable<string> EnumerateFiles(
            string rootDirectory,
            IList<string> includeExtensions,
            bool recurseSubdirectories = false,
            string relativePath = "\\")
            {
            relativePath = relativePath ?? string.Empty;
            // Enumerate files first, then recurse into directories.
            var searchPatterns = includeExtensions.Select(p => "*." + p.Trim().TrimStart('.'));
            foreach (var searchPattern in searchPatterns)
                {
                var filesInCurrentDirectory = Directory.GetFileSystemEntries(rootDirectory, searchPattern,
                    SearchOption.TopDirectoryOnly);
                var filesToProcess = filesInCurrentDirectory.OrderBy(p => p);
                foreach (var fileName in filesToProcess)
                    {
                    var fileNameWithoutPath = Path.GetFileName(fileName);
                    yield return Path.Combine(relativePath, fileNameWithoutPath);
                    }
                }

            if (recurseSubdirectories == false)
                yield break;

            var directories = Directory.GetDirectories(rootDirectory, "*", SearchOption.TopDirectoryOnly);
            foreach (var directory in directories)
                {
                var directoryInfo = new DirectoryInfo(directory);
                var directoryName = directoryInfo.Name; // Directory name without path
                if (directoryName.StartsWith("."))
                    continue; // Don't process directories such as ".git"
                var relative = Path.Combine(relativePath, directoryName);
                var childFiles = EnumerateFiles(directory, includeExtensions, recurseSubdirectories, relative);
                foreach (var childFile in childFiles)
                    {
                    yield return childFile;
                    }
                }
            }
        }
    }